class MessagesController < ApplicationController
  def index
	  @messages = Message.all
  end

  def new
	  @Message = Message.new
  end

  def create
	  Message.create(content: message_params[:content])
    redirect_to root_path
  end

	private
  def message_params
    params.permit(:content)
  end
end
